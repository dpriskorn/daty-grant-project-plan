#+TITLE: Unified Experience Project
Project leader: Dennis Priskorn
Members: Dennis Priskorn, Pellegrino Prevete
* Focus on experience
* People
** Pellegrino
** Dennis
** ...
* Goals
The goal is to create a unified experience to Wikidata. We want to create a prototype for both a desktop and a mobile application that combine search-based statement editor and to deliver a working prototype aft 5 design sprints during 5 weeks and present a working prototype of both the desktop and mobile version on July 1 2021.  Our main focus is that the prototype is accessible, has a high degree of usefullness and include behavioral design patterns.
We are gonna communicate the result in a film where we present the prototype.
If one or both prototypes are approved (comittee) we are gonna go ahead and implement the prototype(s) into a working software product. (So we are gonna start the actual development in July?)
* Usergroups (added)
1) First time users discovering wikidata
2) New and seasoned editors
3) ??
* Our goal group
** Desktop
 Our main focus group for the desktop application is seasoned editors who are not in a hurry, willing to learn and add > 10 statements a month to Wikidata in the last 6 months and use Windows and are 50 years old (AHAHAHHAHAHAHAH).
** Mobile
 Our main focus group is people 20-30 years searching for facts in a hurry who are new to Wikidata and who added 0 statements before to Wikidata and don't really know what a statement or entity is.
 * Effect goals
 ** Desktop
 - feedback mechanism 
 - deep system integration (notifications, search provider, user accounts, data re-usage);
 ** Mobile
 - easy and nice overview of the data the user is looking for
 - fast/instant loading under 600 msec after keypress during search
 - search-based navigation
* Stray ideas
- ability for user scripts like GIMP written in Python
- different search possibility without the ?s ?p ?o box
- constraints in searches?
- power of the search engine is xx
- what is a statement based editor?
- responsive
- light version for mobile through Broadway
- reuse backend between desktop and mobile version. Toolkit can be changed out.
- dead simple to add information is a goal

* Demands specification for the product
** Desktop
- adapt desktop edition to needs of power users
- interactive
- work on different screen sizes (tablet, big external TV screen)
- easy and playful to add statements
** Mobile
- fast flow
- "get out of my way"-ish
- easy and playful to add statements
- avoid nonsense and bloated features
* Demands specification for the project
- public and updated project plan
- public kanban board
- public prototype versions after each sprint
- public whiteboard (viewing)
- plan has to be followed
- 4-5 design sprints with public sprint retrospectives
* Limitations
No coding in this project. This is a design project where we prototype and investigate the feasibility of the ideas.
We don't do login handling in the prototype.
* Expectations / preconditions
We read the project plan and the book Don't make me think by Steve Krug.
We read up on behavioral design of software.
* Analysis of current state and stake holders
**SWOT
***Strengths
- Proficiency in Python
- The project manager has experience with design sprints from before
- 
***Weaknesses
- Not able to meet up
- Cultural differences
***Opportunities
-Learn more about the needs of users
- 
***Threats
- Sickness
- Miro or Prototype tool stops working
- Loss of data
* Milestones, activities and time plan
Link to kanban board xx
** Milestones
- Finish design sprint 1
- Finish design sprint 2
- Finish design sprint 3
- Finish design sprint 4
- Finish design sprint 5 optionally
- Ready demo of both desktop and mobile app.
** Activities
1) Write grant proposal
2) 
